package ru.rtkit.someservice.apiHelper;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;
import ru.rtkit.someservice.Tests.BaseTest;

import static io.restassured.RestAssured.given;


public class PetsStoreApi extends BaseTest {

    public PetsStoreApi() {

        RestAssured.baseURI = URI;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new AllureRestAssured());
    }

    public Response get(String endpoint, ResponseSpecification resp) {

        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint);
    }

    public Response get(String endpoint, ResponseSpecification resp, Object... pathParams) {

        return given()
                .log().all()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .log().all()
                .when()
                .get(endpoint, pathParams);
    }

    public Response post(String endpoint, Object body, ResponseSpecification resp) {

        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

}
