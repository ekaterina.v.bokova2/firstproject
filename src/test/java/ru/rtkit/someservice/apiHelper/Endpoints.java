package ru.rtkit.someservice.apiHelper;

public class Endpoints {

    //pet
    public static final String NEW_PT = "/pet";
    public static final String PET_ID = "/pet/{petId}";

    //store
    public static final String ORDER = "/store/order";
    public static final String ORDER_ID = "/store/order/{orderID}";
}

