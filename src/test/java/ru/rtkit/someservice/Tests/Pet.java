package ru.rtkit.someservice.Tests;

//@Data
public class Pet {
    private Long id;
    private Long petId;
    private Integer quantity;
    private String shipDate;
    private String status;
    private Boolean complete;

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public Long getPetId() {

        return petId;
    }

    public void setPetId(Long petId) {

        this.petId = petId;
    }

    public Integer getQuantity(int i) {

        return quantity;
    }

    public void setQuantity(Integer quantity) {

        this.quantity = quantity;
    }

    public String getShipDate(String s) {

        return shipDate;
    }

    public void setShipDate(String shipDate) {

        this.shipDate = shipDate;
    }

    public String getStatus(String placed) {

        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public Boolean getComplete(boolean b) {

        return complete;
    }

    public void setComplete(Boolean complete) {

        this.complete = complete;
    }
}

