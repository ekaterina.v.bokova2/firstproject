package ru.rtkit.someservice.Tests;

import io.restassured.specification.ResponseSpecification;
import ru.rtkit.someservice.apiHelper.PetsStoreApi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.expect;

public class BaseTest {

    public static ResponseSpecification resp200 = expect().statusCode(200);

    public static PetsStoreApi api;
    public static Properties props;
    public static String URI;


    static {

        InputStream is = ClassLoader.getSystemResourceAsStream("service.properties");
        props = new Properties();
        try {
            props.load(is);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        URI =  props.getProperty("url", "https://petstore.swagger.io/v2");

        api = new PetsStoreApi();

    }

}
