package ru.rtkit.someservice.Tests;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import ru.rtkit.someservice.apiHelper.Endpoints;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestNew extends BaseTest {
    static Stream<String> newNameVariants() {

        return Stream.of("busya", "буся", "BUSYA", "бУся");
    }

    @ParameterizedTest
    @MethodSource("newNameVariants")
    @DisplayName("Проверка новых регистров")
    @Description("Проверка новой опции: возможности добавления имени питомца в разных регистрах")
    void addNewName(String argument) {

        JSONObject bodyJO = new JSONObject()
                .put("name", argument);
        long id = addNewPetToStore(bodyJO.toString(), argument);
        String newName = api.get(Endpoints.PET_ID, resp200, id)
                .jsonPath()
                .getString("name");
        assertThat(argument, Matchers.equalTo(newName));
    }

    @Test
    @DisplayName("Провека добавления питомца")
    @Description("Добавление нового питомца и проверка его наличия в базе")
    void findPetTest() {

        String petName = "Bars";
        JSONObject bodyJO = new JSONObject()
                .put("id", "489")
                .put("сategory", new JSONObject().put("name", "cat"))
                .put("name", petName)
                .put("status", "available");

        long id = addNewPetToStore(bodyJO.toString(), petName);
        String actualName = api.get(Endpoints.PET_ID, resp200, id)
                .jsonPath()
                .getString("name");
        Assertions.assertEquals(petName, actualName);
    }

    @Step("Добавление питомца {petNames} в магазин ")
    long addNewPetToStore(String pet, String petNames) {

        return api.post(Endpoints.NEW_PT, pet, resp200)
                .jsonPath()
                .getLong("id");
    }

    @Test
    @DisplayName("Наличие питомца в магазине")
    @Description("Добавление нового питомца и проверка его наличия в магазине")
    void checkPetStore() {

        JSONObject bodyJO = new JSONObject()
                .put("name", "Барсик");
        long id = addNewPetToStore(bodyJO.toString(), "Барсик");
        JSONObject bodyNextJO = new JSONObject()
                .put("id", 0)
                .put("petId", id)
                .put("quantity", 0)
                .put("shipDate", "2022-07-27T16:20:15.497Z")
                .put("status", "placed")
                .put("complete", true);

        Boolean complete = api.post(Endpoints.ORDER, bodyNextJO.toString(), resp200)
                .jsonPath()
                .getBoolean("complete");
        assertTrue(complete == true);
    }

    @Test
    void workWithProperties() {

        System.out.println(props);
    }

    @Test
    @DisplayName("Наличие питомца в магазине")
    @Description("Добавление питомца в магазин. Использование нового метода в тесте")
    void checkPetStoreNew() {

        Pet petOne = new Pet();
        petOne.setId(26L);
        petOne.setPetId(38L);
        petOne.getQuantity(26);
        petOne.getShipDate("2022-08-18T05:57:10.369Z");
        petOne.getStatus("placed");
        petOne.getComplete(true);

        api.post(Endpoints.ORDER, petOne, resp200);
        Pet petActual = api.get(Endpoints.ORDER_ID, resp200, petOne.getId()).as(Pet.class);
        Assertions.assertEquals(petOne.getId(), petActual.getId());
    }

}
