package ru.rtkit.someservice.Tests;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase {
    String pathToDB =
            getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();
    String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Test
    void get () {
        String query = "SELECT * FROM Genres";
        List<String> genres = new ArrayList<>();

        try {
            Connection conn = DriverManager.getConnection(dbUrl);
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                genres.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println(genres);
    }

    @Test
    void getNext () {
        String query = "SELECT * FROM Invoices limit 1";
        List<String> invoices = new ArrayList<>();

        try {
            Connection conn = DriverManager.getConnection(dbUrl);
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                invoices.add(rs.getString("billingCity"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println(invoices);
    }

    @Test
    void getSecond () {
        String query = "SELECT * FROM tracks WHERE genreId IN (SELECT genreId FROM genres WHERE name LIKE 'pop')  ";
        List<String> tracks = new ArrayList<>();

        try {
            Connection conn = DriverManager.getConnection(dbUrl);
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tracks.add(rs.getString("trackId"));
                tracks.add(rs.getString("albumId"));
                tracks.add(rs.getString("mediaTypeId"));
                tracks.add(rs.getString("name"));
                tracks.add(rs.getString("composer"));
                tracks.add(rs.getString("milliseconds"));
                tracks.add(rs.getString("bytes"));
                tracks.add(rs.getString("unitPrice"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println(tracks);
    }

}
